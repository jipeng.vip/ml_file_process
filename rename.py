# coding=utf-8
import sys
import os

# 要批量修改文件名的目录
path = '/Users/jay/code/dev/python/ml_file_process/test_data/Annotations/'
# path = '/Users/jay/code/dev/python/ml_file_process/test_data/JPEGImages/'

#获取该目录下所有文件，存入列表中
f = os.listdir(path)
f.sort()

# 文件名编号起始值
start_no = 0

# 文件名长度，左侧补0
file_seq_length = 6

n = 0
for i in f:

    #设置旧文件名（就是路径+文件名）
    oldname = path+f[n]

    #设置新文件名 000001.xxx
    newname = path+str(n+1+start_no).zfill(file_seq_length)+'.'+os.path.splitext(f[n])[-1][1:]

    if os.path.exists(newname):
        print('new file name '+ newname+ ' already exist!')
        sys.exit(0)

    #用os模块中的rename方法对文件改名
    os.rename(oldname, newname)
    print(oldname, '===>', newname)
    n += 1
