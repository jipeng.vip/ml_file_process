#!/usr/bin/python
# -*- coding=utf-8 -*-
# author : jay
# date: 2019/01/29
# version: 0.1
# 参考： https://blog.csdn.net/wklken/article/details/7603071

import os

from xml.etree.ElementTree import ElementTree,Element

path = '/Users/jay/code/dev/python/ml_file_process/test_data/Annotations/'
image_path = '/Users/jay/code/dev/python/ml_file_process/test_data/JPEGImages/'
folder = 'pd5f0'

#获取该目录下所有文件，存入列表中
f = os.listdir(path)
f.sort()

def read_xml(in_path):
    '''读取并解析xml文件
       in_path: xml路径
       return: ElementTree'''
    tree = ElementTree()
    tree.parse(in_path)
    return tree
 
def write_xml(tree, out_path):
    '''将xml文件写出
       tree: xml树
       out_path: 写出路径'''
    tree.write(out_path, encoding="utf-8",xml_declaration=True)

def find_nodes(tree, path):
    '''查找某个路径匹配的所有节点
       tree: xml树
       path: 节点路径'''
    return tree.findall(path)

def change_node_text(nodelist, text, is_add=False, is_delete=False):
    '''改变/增加/删除一个节点的文本
       nodelist:节点列表
       text : 更新后的文本'''
    for node in nodelist:
        if is_add:
            node.text += text
        elif is_delete:
            node.text = ""
        else:
            node.text = text

n = 0
for i in f:
    oldname = path+f[n]
    print(oldname)
    # 读取xml文件
    tree = read_xml(oldname)

    # 找到 节点
    folder_nodes = find_nodes(tree, "folder")
    filename_nodes = find_nodes(tree, "filename")
    path_nodes = find_nodes(tree, "path")

    # 文件名  不包含后缀
    file_name = os.path.splitext(f[n])[0]

    # 修改节点内容
    change_node_text(folder_nodes, folder)
    change_node_text(filename_nodes, file_name)
    change_node_text(path_nodes, image_path + file_name + '.jpg')

    # 回写XML
    write_xml(tree, oldname)
    n += 1